import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{FlatSpec, Matchers}
import pro.sholokhov.api.rest.RestRoutes
import pro.sholokhov.api.validation.ValidationDirective.BadRequestRejection

/**
  * @author Artem Sholokhov
  */
class RestSpec extends FlatSpec with Matchers with ScalatestRouteTest with RestRoutes {

  "Rest API" should "handle POST UserRequests" in {
    val user = HttpEntity(
        ContentTypes.`application/json`
      , """
          |{
          | "userName": "David",
          | "email": "david@gmail.com",
          | "age": "28",
          | "gender": "1"
          |}
        """.stripMargin
    )
    Post("/register").withEntity(user) ~> routes ~> check {
      status should be(StatusCodes.OK)
    }
  }

  it should "handle POST ActionRequests" in {
    val action = HttpEntity(
        ContentTypes.`application/json`
      , """
          |{
          | "userId": "9797345",
          | "videoId": "4324556",
          | "actionId": "1"
          |}
        """.stripMargin
    )
    Post("/action").withEntity(action) ~> routes ~> check {
      rejection shouldBe BadRequestRejection(
        List(
            "userId does not exist"
          , "videoId does not exist"
        ))
    }
  }

}
