import akka.actor.ActorSystem
import cats.data.NonEmptyList
import cats.data.Validated.{Invalid, Valid}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import pro.sholokhov.domain.Domain
import pro.sholokhov.domain.Domain.{ActionRequest, UserRequest}

import scala.concurrent.Await
import scala.concurrent.duration._

import scala.language.postfixOps

/**
  * @author Artem Sholokhov
  */
class ValidatorSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  import pro.sholokhov.api.validation.Validator
  import pro.sholokhov.api.validation.ValidatorInstances._

  private val sys = ActorSystem("spec-sys")

  override protected def afterAll(): Unit =
    Await.result(sys.terminate, 3 seconds)

  "Validator" should "pass valid UserRequest" in {
    val r = UserRequest("David", "david@gmail.com", 28, 1)
    val v = implicitly[Validator[UserRequest]]
    v.validate(r) shouldBe Valid(r)
  }

  it should "reject invalid UserRequest due to invalid age" in {
    val r = UserRequest("David", "david@gmail.com", 2, 1)
    val v = implicitly[Validator[UserRequest]]
    v.validate(r) shouldBe Invalid(NonEmptyList.of("age is not valid"))
  }

  it should "accumulate errors for UserRequset" in {
    val r = UserRequest("", "", 2, 4)
    val v = implicitly[Validator[UserRequest]]
    val errors = NonEmptyList.of(
        "name should be non empty"
      , "email is not valid"
      , "age is not valid"
      , "gender is not valid"
    )
    v.validate(r) shouldBe Invalid(errors)
  }

  it should "reject pass valid ActionRequest" in {
    val (userId, videoId) = Domain.createUser(UserRequest("David", "david@gmail.com", 28, 1), sys)
    val r                 = ActionRequest(userId, videoId, 3)
    val v                 = implicitly[Validator[ActionRequest]]
    v.validate(r) shouldBe Valid(r)
  }

  it should "accumulate errors and reject invalid ActionRequest" in {
    val r = ActionRequest(100L, 200L, 4)
    val v = implicitly[Validator[ActionRequest]]
    val errors = NonEmptyList.of(
        "userId does not exist"
      , "videoId does not exist"
      , "action is not valid"
    )
    v.validate(r) shouldBe Invalid(errors)
  }

}
