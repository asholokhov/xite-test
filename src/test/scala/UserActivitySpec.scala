import akka.actor.{ActorIdentity, ActorSystem, Identify}
import akka.util.Timeout
import org.scalatest.{Assertions, BeforeAndAfterAll, FlatSpec, Matchers}
import pro.sholokhov.domain.Domain
import pro.sholokhov.domain.Domain._

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Random

/**
  * @author Artem Sholokhov
  */
class UserActivitySpec extends FlatSpec with Matchers with Assertions with BeforeAndAfterAll {

  import akka.pattern.ask

  private val sys               = ActorSystem("user-activity-spec")
  implicit val timeout: Timeout = Timeout(1 second)

  override protected def afterAll(): Unit =
    Await.result(sys.terminate, 3 seconds)

  "UserActivitySpec" should "be created for new user" in {
    val (userId, _) = Domain.createUser(UserRequest("Test", "zzz@mail.com", 35, 1), sys)
    val actorName   = Domain.actorName(userId)
    val f = for {
      ref <- sys.actorSelection("user/" + actorName).resolveOne
      ans <- ref ? Identify("Hello")
    } yield {
      ans shouldBe ActorIdentity("Hello", Some(ref))
    }
    Await.result(f, 1 second)
  }

  it should "change video in case of valid request" in {
    val (userId, videoId) = Domain.createUser(UserRequest("New", "user@mail.com", 20, 1), sys)
    val actorName         = Domain.actorName(userId)
    val f = for {
      ref <- sys.actorSelection("user/" + actorName).resolveOne
      ans <- ref ? ActionRequest(userId, videoId, 1)
    } yield {
      ans match {
        case VideoChanged(_) => succeed
        case _               => fail("Unexpected answer from UserActivity actor")
      }
    }
    Await.result(f, 1 second)
  }

  it should "reject action in case of invalid videoId supplied" in {
    val (userId, _) = Domain.createUser(UserRequest("Ann", "ann@mail.com", 24, 2), sys)
    val actorName   = Domain.actorName(userId)
    val f = for {
      ref <- sys.actorSelection("user/" + actorName).resolveOne
      ans <- ref ? ActionRequest(userId, Random.nextLong, 1)
    } yield {
      ans match {
        case InvalidVideoIdSupplied(_, _) => succeed
        case _                            => fail("Unexpected answer from UserActivity actor")
      }
    }
    Await.result(f, 1 second)
  }

  it should "handle invalid routing" in {
    val (userId1, _)        = Domain.createUser(UserRequest("Ann", "ann@mail.com", 24, 2), sys)
    val (userId2, videoId2) = Domain.createUser(UserRequest("Jason", "jason@mail.com", 24, 1), sys)
    val actorName           = Domain.actorName(userId2)
    val f = for {
      ref <- sys.actorSelection("user/" + actorName).resolveOne
      ans <- ref ? ActionRequest(userId1, videoId2, 1)
    } yield {
      ans match {
        case InvalidRoutingOccurred(_) => succeed
        case _                         => fail("Unexpected answer from UserActivity actor")
      }
    }
    Await.result(f, 1 second)
  }

}
