package pro.sholokhov.domain

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

import akka.actor.{ActorRef, ActorSystem}
import pro.sholokhov.core.UserActivity

import scala.util.Random

/**
  * @author Artem Sholokhov
  */
object Domain {

  type UserId   = Long
  type VideoId  = Long
  type ActionId = Int

  /** Preconfigured list of videos. Warning: possible duplicates, but chance is very small. */
  private val watches = {
    val m = new ConcurrentHashMap[VideoId, Long]()
    Seq.fill(10)(Math.abs(Random.nextLong)).foreach(id => m.put(id, 0))
    m
  }

  def videoExists(videoId: VideoId): Boolean = watches.containsKey(videoId)

  /**
    * Chooses least watched video from the list.
    *
    * For the sake of simplicity it implemented in straightforward way via full scan. However, this
    * approach has two significant flaws:
    *   1. Current runtime complexity O(N). It would be nice to implement and support here Heap (Priority Queue)
    *      with O(log(n)) operations.
    *   2. Since it is a concurrent collection result may not be fair.
    */
  def nextVideoId(): VideoId = {
    var resultVideoId: VideoId = -1
    watches.forEach {
      case (vid, count) =>
        if (resultVideoId == -1 || watches.get(resultVideoId) > count)
          resultVideoId = vid
    }
    watches.compute(resultVideoId, (_, old) => old + 1)
    resultVideoId
  }

  /** Simple way to generate unique user id's in sequential order */
  private val seqId = new AtomicLong(0)
  private val users = new ConcurrentHashMap[UserId, ActorRef]()

  def nextUserId(): UserId                = seqId.incrementAndGet()
  def userExists(userId: UserId): Boolean = users.containsKey(userId)
  def actorName(userId: UserId): String   = Seq("UserActivity", userId).mkString("-")

  def createUser(r: UserRequest, sys: ActorSystem): (UserId, VideoId) = {
    val userId  = nextUserId()
    val videoId = nextVideoId()
    val ref     = sys.actorOf(UserActivity.props(r, userId, videoId), actorName(userId))
    users.put(userId, ref)
    (userId, videoId)
  }

  /** Represents request for register endpoint */
  final case class UserRequest(userName: String, email: String, age: Int, gender: Int)

  /** Represents action request for action endpoint */
  final case class ActionRequest(userId: UserId, videoId: VideoId, actionId: ActionId)

  /** Common response for both endpoints */
  final case class XiteResponse(userId: UserId, videoId: VideoId)

  /** Represents allowed actions */
  sealed abstract class Action(id: Int)
  case object Like extends Action(1)
  case object Skip extends Action(2)
  case object Play extends Action(3)

  /** Messages for internal actors communication */
  sealed trait XiteActorMessage
  final case class VideoChanged(newVideoId: VideoId) extends XiteActorMessage
  final case class InvalidVideoIdSupplied(currentVideoId: VideoId, invalidId: VideoId)
      extends XiteActorMessage
  final case class InvalidRoutingOccurred(incorrectReceiverId: UserId) extends XiteActorMessage

}
