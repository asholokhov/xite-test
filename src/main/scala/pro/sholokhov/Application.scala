package pro.sholokhov

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.slf4j.LoggerFactory
import pro.sholokhov.api.Gateway

import scala.concurrent.duration._
import scala.concurrent.{Await, Promise}
import scala.language.postfixOps

/**
  * @author Artem Sholokhov
  */
object Application extends App {

  private val logger = LoggerFactory.getLogger(this.getClass)

  /** This is supposed to use with start / stop semantics in the future */
  private val stopNow = Promise[Unit]

  implicit val sys: ActorSystem       = ActorSystem("xite")
  implicit val mat: ActorMaterializer = ActorMaterializer()

  try {
    logger.info("*** Initializing ***")
    sys.actorOf(Gateway.props(mat), "gateway")
    Await.result(stopNow.future, Duration.Inf)
  } catch {
    case e: Throwable =>
      logger.error(s"Application crashed: $e")
      System.exit(1)
  } finally {
    Await.result(sys.terminate(), 3 seconds)
  }

}
