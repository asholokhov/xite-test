package pro.sholokhov.api.handlers

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import pro.sholokhov.domain.Domain

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * @author Artem Sholokhov
  */
object ActionsHandler {

  import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
  import io.circe.generic.auto._
  import io.circe.syntax._
  import pro.sholokhov.api.validation.ValidationDirective._
  import pro.sholokhov.api.validation.ValidatorInstances._
  import pro.sholokhov.domain.Domain._

  import scala.language.postfixOps

  implicit val timeout: Timeout = Timeout(1 second)

  def handleAction: Route =
    entity(as[ActionRequest]) { raw =>
      validate(raw).apply { action =>
        extractActorSystem { sys =>
          val actorName = Domain.actorName(action.userId)
          val rawResponse = for {
            ref <- sys.actorSelection("user/" + actorName).resolveOne
            res <- (ref ? action).mapTo[XiteActorMessage]
          } yield
            res match {
              case VideoChanged(newVideoId) =>
                complete(
                  HttpEntity(ContentTypes.`application/json`,
                             XiteResponse(action.userId, newVideoId).asJson.toString))
              case InvalidVideoIdSupplied(currentVideoId, invalidId) =>
                reject(
                  BadRequestRejection(
                    List(s"Invalid video supplied: user watching $currentVideoId, got $invalidId instead")))
              case InvalidRoutingOccurred(_) =>
                reject(BadRequestRejection(List(s"Something went wrong")))
            }

          val response =
            rawResponse.recover {
              case _: Throwable => reject(BadRequestRejection(List("User actor doesn't presented in system")))
            }

          Await.result(response, 3 seconds)
        }
      }
    }

}
