package pro.sholokhov.api.handlers

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

/**
  * @author Artem Sholokhov
  */
object UsersHandler {

  import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
  import io.circe.generic.auto._
  import io.circe.syntax._
  import pro.sholokhov.api.validation.ValidationDirective._
  import pro.sholokhov.api.validation.ValidatorInstances._
  import pro.sholokhov.domain.Domain._

  def register: Route =
    entity(as[UserRequest]) { raw =>
      validate(raw).apply { req =>
        extractActorSystem { sys =>
          val (userId, videoId) = createUser(req, sys)
          complete(HttpEntity(ContentTypes.`application/json`, XiteResponse(userId, videoId).asJson.toString))
        }
      }
    }

}
