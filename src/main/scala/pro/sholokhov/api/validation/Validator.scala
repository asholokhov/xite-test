package pro.sholokhov.api.validation

import pro.sholokhov.domain.Domain.{ActionRequest, UserId, UserRequest, VideoId}
import cats.data._
import cats.implicits._
import pro.sholokhov.domain.Domain

/**
  * @author Artem Sholokhov
  */
trait Validator[T] {

  type ErrorType           = String
  type ValidationResult[A] = ValidatedNel[ErrorType, A]

  /**
    * W3C recommended way to validate emails: https://www.w3.org/TR/html5/forms.html#valid-e-mail-address
    * Actually, Play Framework use the same approach to validate email addresses.
    */
  private val emailRegex =
    """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".r

  /** Validates entity with rules set */
  def validate(entity: T): ValidationResult[T]

  //

  def checkName(name: String): ValidationResult[String] =
    Either.cond(name.nonEmpty, name, "name should be non empty").toValidatedNel

  def checkEmail(email: String): ValidationResult[String] = {
    val isValid = email match {
      case null                                          => false
      case e if e.trim.isEmpty                           => false
      case e if emailRegex.findFirstMatchIn(e).isDefined => true
      case _                                             => false
    }
    Either.cond(isValid, email, "email is not valid").toValidatedNel
  }

  def checkAge(age: Int): ValidationResult[Int] =
    Either.cond(age >= 5 && age <= 120, age, "age is not valid").toValidatedNel

  def checkGender(g: Int): ValidationResult[Int] =
    Either.cond(g == 1 || g == 2, g, "gender is not valid").toValidatedNel

  def checkUserExists(userId: UserId): ValidationResult[UserId] =
    Either.cond(Domain.userExists(userId), userId, "userId does not exist").toValidatedNel

  def checkVideoExists(videoId: VideoId): ValidationResult[VideoId] =
    Either.cond(Domain.videoExists(videoId), videoId, "videoId does not exist").toValidatedNel

  def checkAction(a: Int): ValidationResult[Int] =
    Either.cond(a >= 1 && a <= 3, a, "action is not valid").toValidatedNel

}

object ValidatorInstances {

  implicit val userValidator: Validator[UserRequest] = new Validator[UserRequest] {
    override def validate(entity: UserRequest): ValidationResult[UserRequest] =
      (checkName(entity.userName), checkEmail(entity.email), checkAge(entity.age), checkGender(entity.gender))
        .mapN(UserRequest)
  }

  implicit val actionValidator: Validator[ActionRequest] = new Validator[ActionRequest] {
    override def validate(entity: ActionRequest): ValidationResult[ActionRequest] =
      (checkUserExists(entity.userId), checkVideoExists(entity.videoId), checkAction(entity.actionId))
        .mapN(ActionRequest)
  }

}
