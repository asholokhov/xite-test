package pro.sholokhov.api.validation

import akka.http.scaladsl.server.Rejection
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.Directives._

/**
  * @author Artem Sholokhov
  */
object ValidationDirective {

  import cats.data.Validated._

  /** Represents response with accumulated validation errors */
  final case class BadRequestRejection(errors: Seq[String]) extends Rejection

  def validate[A](entity: A)(implicit v: Validator[A]): Directive1[A] =
    v.validate(entity) match {
      case Valid(e)     => provide(e)
      case Invalid(ers) => reject(BadRequestRejection(ers.toList))
    }

}
