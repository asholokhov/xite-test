package pro.sholokhov.api

import akka.actor.{Actor, ActorSystem, Props}
import akka.stream.ActorMaterializer
import akka.http.scaladsl.Http
import org.slf4j.{Logger, LoggerFactory}
import pro.sholokhov.Config
import pro.sholokhov.api.rest.RestRoutes

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContextExecutor

/**
  * @author Artem Sholokhov
  */
class Gateway(implicit m: ActorMaterializer) extends Actor with RestRoutes {

  implicit val dis: ExecutionContextExecutor = context.dispatcher
  implicit val sys: ActorSystem              = context.system

  private val bindings: ListBuffer[Http.ServerBinding] = ListBuffer.empty
  private val log: Logger                              = LoggerFactory.getLogger(this.getClass)

  override def receive: Receive = {
    case boundEvent: Http.ServerBinding =>
      log.info(s"Gateway started at: ${boundEvent.localAddress.toString}")
      bindings += boundEvent
  }

  override def preStart(): Unit = bind()
  override def postStop(): Unit = bindings foreach (_.unbind())

  private def bind(): Unit = {
    val selfRef = self
    Http().bindAndHandle(routes, Config.host, Config.port).foreach(bound => selfRef ! bound)
  }

}

object Gateway {
  def props(implicit m: ActorMaterializer): Props =
    Props(classOf[Gateway], m)
}
