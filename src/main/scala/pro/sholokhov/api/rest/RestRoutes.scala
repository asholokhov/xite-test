package pro.sholokhov.api.rest

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{RejectionHandler, Route}
import pro.sholokhov.api.handlers.{ActionsHandler, UsersHandler}
import pro.sholokhov.api.validation.ValidationDirective.BadRequestRejection

/**
  * @author Artem Sholokhov
  */
trait RestRoutes {

  import io.circe.syntax._
  import io.circe.generic.auto._

  implicit def rejectionHandler: RejectionHandler =
    RejectionHandler
      .newBuilder()
      .handle {
        case response@BadRequestRejection(_) =>
          complete((StatusCodes.BadRequest, response.asJson.toString))
      }
      .result()

  val routes: Route =
    pathPrefix("register")(post(UsersHandler.register)) ~
      pathPrefix("action")(post(ActionsHandler.handleAction))

}
