package pro.sholokhov.core

import akka.actor.{Actor, Props}
import org.slf4j.LoggerFactory
import pro.sholokhov.domain.Domain
import pro.sholokhov.domain.Domain._

/**
  * @author Artem Sholokhov
  */
class UserActivity(private val user: UserRequest, userId: Long, videoId: Long) extends Actor {

  private val logger            = LoggerFactory.getLogger(this.getClass)
  override def receive: Receive = trackActivity(videoId)

  /** Internal implementation for keep state immutable. Don't take action id into account due to simplicity. */
  private def trackActivity(lastVideoId: Long): Receive = {
    case ActionRequest(uid, vid, _) =>
      uid match {
        case _ if uid == userId =>
          if (vid == lastVideoId) {
            val newVideoId = Domain.nextVideoId()
            context.become(trackActivity(newVideoId))
            sender() ! VideoChanged(newVideoId)
            logger.info(s"User $userId watching $newVideoId now.")
          } else {
            logger.error(s"Incorrect videoId: user ($userId) watching $lastVideoId. Got $vid instead.")
            sender() ! InvalidVideoIdSupplied(lastVideoId, vid)
          }
        case _ =>
          sender() ! InvalidRoutingOccurred(userId)
          logger.error(s"Incorrect action routing: actor for user $userId received message for user $uid.")
      }
    case _ =>
      logger.info(s"User $userId watching video $lastVideoId.")
  }

}

object UserActivity {
  def props(user: UserRequest, userId: Long, videoId: Long): Props =
    Props(classOf[UserActivity], user, userId, videoId)
}
