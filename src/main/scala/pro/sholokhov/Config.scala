package pro.sholokhov

import com.typesafe.config.{ConfigFactory, Config => Cfg}

/**
  * @author Artem Sholokhov
  */
object Config {
  lazy val xiteConfig: Cfg = ConfigFactory.load().getConfig("xite")

  lazy val host: String = xiteConfig.getString("host")
  lazy val port: Int    = xiteConfig.getInt("port")
}
