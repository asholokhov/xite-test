import sbt._
import Keys._

// versions
lazy val scalaV     = "2.12.4"
lazy val akkaV      = "2.5.11"
lazy val akkaHttpV  = "10.1.0"
lazy val catsV      = "1.0.1"
lazy val slf4jV     = "1.7.22"
lazy val log4jV     = "2.8.2"
lazy val circeV     = "0.9.2"
lazy val akkaCirceV = "1.20.0"
lazy val scalaTestV = "3.0.5"

// common settings
lazy val commonSettings = Seq(
    scalaVersion := scalaV
  , scalacOptions := Seq(
      "-encoding"
    , "UTF-8"
    , "-deprecation"
    , "-explaintypes" // Explain type errors in more detail.
    , "-unchecked"
    , "-feature" // Emit warning and location for usages of features that should be imported explicitly.
    , "-language:higherKinds"
    , "-language:existentials"
    , "-language:implicitConversions"
    , "-Xfuture"           // Turn on future language features.
    , "-Xlint:stars-align" // Pattern sequence wildcard must align with sequence component.
    , "-Ywarn-dead-code"
    , "-Ywarn-unused-import"
    , "-Ypartial-unification"
    , "-Ypatmat-exhaust-depth"
    , "40"
    , "-opt:l:method"
  )
  , organization := "xite"
  , version := "0.1"
)

// backend project
lazy val xiteApp =
  Project(id = "xite-app", base = file("."))
    .settings(commonSettings: _*)
    .settings(
        name := "xite-app"
      , mainClass in Compile := Some("pro.sholokhov.Application")
      , libraryDependencies ++= Seq(
          "com.typesafe.akka"        %% "akka-actor"          % akkaV
        , "com.typesafe.akka"        %% "akka-stream"         % akkaV
        , "com.typesafe.akka"        %% "akka-slf4j"          % akkaV
        , "com.typesafe.akka"        %% "akka-testkit"        % akkaV
        , "com.typesafe.akka"        %% "akka-stream-testkit" % akkaV
        , "com.typesafe.akka"        %% "akka-http-core"      % akkaHttpV
        , "com.typesafe.akka"        %% "akka-http"           % akkaHttpV
        , "com.typesafe.akka"        %% "akka-http-testkit"   % akkaHttpV
        , "io.circe"                 %% "circe-core"          % circeV
        , "io.circe"                 %% "circe-generic"       % circeV
        , "io.circe"                 %% "circe-parser"        % circeV
        , "de.heikoseeberger"        %% "akka-http-circe"     % akkaCirceV
        , "org.typelevel"            %% "cats-core"           % catsV
        , "org.slf4j"                % "slf4j-api"            % slf4jV
        , "org.slf4j"                % "jcl-over-slf4j"       % slf4jV
        , "org.slf4j"                % "slf4j-simple"         % slf4jV
        , "org.apache.logging.log4j" % "log4j-jul"            % log4jV
        , "org.apache.logging.log4j" % "log4j-api"            % log4jV
        , "org.apache.logging.log4j" % "log4j-core"           % log4jV
        , "org.scalactic"            %% "scalactic"           % scalaTestV
        , "org.scalatest"            %% "scalatest"           % scalaTestV % "test"
      )
    )
